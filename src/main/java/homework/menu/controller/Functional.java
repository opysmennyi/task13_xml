package homework.menu.controller;

@FunctionalInterface
public interface Functional {
    void run() throws Exception;
}
