package homework.menu.view;

import homework.constants.Constants;
import homework.menu.model.InitMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Output {
    private static Logger LOG = LogManager.getLogger(Output.class);

    public static void outputMenu() {
        LOG.info(Constants.MENU);
        InitMenu.menu.values().forEach(LOG::trace);
    }
}
