package homework.menu.model;

import homework.constants.Constants;
import homework.menu.controller.Functional;
import homework.task1.task.controller.PingPong;
import homework.task2.task.controller.RunFibonacci;
import homework.task4.model.ThreadFibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class InitMenu {
    public static Object sync = new Object();

    public static final Scanner INPUT = new Scanner(System.in);
    public static final Logger LOG = LogManager.getLogger(OpenMenu.class);

    public static Map<String, String> menu;
    public static Map<String, Functional> methodsMenu;

    public static void initMenu() {
        Thread t1 = new Thread(() -> {
            synchronized (sync) {
                menu = new LinkedHashMap<>();
                methodsMenu = new LinkedHashMap<>();
                menu.put("1", Constants.INSERT + "1" + Constants.FOR_TASK + "1");
                menu.put("2", Constants.INSERT + "2" + Constants.FOR_TASK + "2");
                menu.put("3", Constants.INSERT + "3" + Constants.FOR_TASK + "3");
                menu.put("4", Constants.INSERT + "4" + Constants.FOR_TASK + "4");
                menu.put("5", Constants.INSERT + "5" + Constants.FOR_TASK + "5");
                menu.put("6", Constants.INSERT + "6" + Constants.FOR_TASK + "6");
                menu.put("7", Constants.INSERT + "7" + Constants.FOR_TASK + "7");
                menu.put("8", Constants.INSERT + "8" + Constants.FOR_TASK + "8");
                menu.put("0", Constants.INSERT + "0 for Close homework.menu");

                methodsMenu.put("1", PingPong::playPingPong);
                methodsMenu.put("2", RunFibonacci::runFibonacciNumbers);
//        methodsMenu.put("3", MainTask3::runTask3);
                methodsMenu.put("4", ThreadFibonacci::threadFibonacci);
//        methodsMenu.put("5", MainTask5::runTask5);
//        methodsMenu.put("6", MainTask6::runTask6);
//        methodsMenu.put("7", MainTask7::runTask7);
//        methodsMenu.put("8", RunTask8::runTask8);
                methodsMenu.put("0", Exit::exit);

                sync.notify();
            }
        });
        OpenMenu openMenu = new OpenMenu();
        t1.start();
        openMenu.t2.start();
        try {
            openMenu.t2.join();
            t1.join();
        } catch (InterruptedException e) {
        }
    }
}
