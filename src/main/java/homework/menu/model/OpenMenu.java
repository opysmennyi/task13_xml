package homework.menu.model;

import homework.constants.Constants;
import homework.menu.view.Output;

import java.io.IOException;

public class OpenMenu extends Thread {
    Thread t2 = new Thread(() -> {
        synchronized (InitMenu.sync) {
            String key;
            String wrongInput = "\nWrong input";
            String choseTask = "Chose a task to run";
            String error = "Bye-bye that is ERROR";
            String crashed = "\nOMG upload new file please this one crashed";
            String classmistake = "\nThat is your mistake you've could create this class but you didn't";
            while (true) {
                try {
                    Output.outputMenu();
                    InitMenu.LOG.trace(choseTask);
                    key = InitMenu.INPUT.nextLine();
                    InitMenu.LOG.trace(Constants.NEW_EMPTY_LINE);
                    InitMenu.methodsMenu.get(key).run();
                } catch (NullPointerException e) {
                    InitMenu.LOG.warn(wrongInput);
                } catch (IOException e) {
                    InitMenu.LOG.error(crashed);
                } catch (ClassNotFoundException e) {
                    InitMenu.LOG.error(classmistake);
                } catch (Exception e) {
                    InitMenu.LOG.error(error);
                }
                try {
                    InitMenu.sync.wait(1500);
                } catch (InterruptedException e) {
                }
            }
        }
    });
}

