package homework.task1.model;

public class Ingridients {
    private double shugar;
    private double chocolate;
    private double nut;
    private double water;

    public Ingridients(double shugar, double chocolate, double nut, double water) {
        this.shugar = shugar;
        this.chocolate = chocolate;
        this.nut = nut;
        this.water = water;
    }

    public Ingridients() {
    }

    public double getShugar() {
        return shugar;
    }

    public void setShugar(double shugar) {
        this.shugar = shugar;
    }

    public double getChocolate() {
        return chocolate;
    }

    public void setChocolate(double chocolate) {
        this.chocolate = chocolate;
    }

    public double getNut() {
        return nut;
    }

    public void setNut(double nut) {
        this.nut = nut;
    }

    public double getWater() {
        return water;
    }

    public void setWater(double water) {
        this.water = water;
    }

    @Override
    public String toString() {
        return "Ingridients{" +
                "shugar=" + shugar +
                ", chocolate=" + chocolate +
                ", nut=" + nut +
                ", water=" + water +
                '}';
    }
}