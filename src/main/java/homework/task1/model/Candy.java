package homework.task1.model;

public class Candy {

    private String name;
    private int energy;
    private String type;
    private String ingredients;
    private String value;
    private String prodaction;

    public Candy() {
    }

    public Candy(String name, int energy, String type, String ingredients, String value, String prodaction) {
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.value = value;
        this.prodaction = prodaction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProdaction() {
        return prodaction;
    }

    public void setProdaction(String prodaction) {
        this.prodaction = prodaction;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", energy=" + energy +
                ", type='" + type + '\'' +
                ", ingredients=" + ingredients +
                ", value=" + value +
                ", prodaction='" + prodaction + '\'' +
                '}';
    }
}
