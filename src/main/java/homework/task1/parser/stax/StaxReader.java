package homework.task1.parser.stax;

import homework.task1.model.Candy;
import homework.task1.model.Ingridients;
import homework.task1.model.Value;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxReader {
    public static List<Candy> parseCandies(File xml, File xsd) {
        List<Candy> candies = new ArrayList<>();
        Candy candy = null;
        Ingridients ingridients = null;
        Value value = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "candy":
                            candy = new Candy();

                            Attribute idAttr = startElement.getAttributeByName(new QName("name"));
                            if (idAttr != null) {
                                candy.setName(xmlEvent.asCharacters().getData());
                            }
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "energy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "prodaction":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setProdaction(xmlEvent.asCharacters().getData());
                            break;
                        case "shugar":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingridients.setShugar(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "chocolate":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingridients.setChocolate(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "nut":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingridients.setNut(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "water":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingridients.setWater(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            ;
                            break;
                        case "proteins":
                            xmlEvent = xmlEventReader.nextEvent();
                            value.setProteins(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "fats":
                            xmlEvent = xmlEventReader.nextEvent();
                            value.setFats(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "carbonohydrates":
                            xmlEvent = xmlEventReader.nextEvent();
                            value.setCarbohydrates(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            candy.setValue(xmlEvent.asCharacters().getData());
                            break;
                        case "ingridients":
                            xmlEvent = xmlEventReader.nextEvent();
                            candy.setIngredients(xmlEvent.asCharacters().getData());
                            break;
                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("candy")) {
                        candies.add(candy);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return candies;
    }
}
