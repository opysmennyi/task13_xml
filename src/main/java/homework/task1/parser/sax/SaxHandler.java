package homework.task1.parser.sax;

import homework.task1.model.Candy;
import homework.task1.model.Ingridients;
import homework.task1.model.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    private List<Candy> candies = new ArrayList<>();
    private Candy candy = null;
    private Ingridients ingredients = null;
    private Value value = null;

    private boolean bName = false;
    private boolean bEnergy = false;
    private boolean bType = false;
    private boolean bProdaction = false;
    private boolean bShugar = false;
    private boolean bChocolate = false;
    private boolean bNut = false;
    private boolean bWater = false;
    private boolean bProteins = false;
    private boolean bFats = false;
    private boolean bCarbohydrates = false;
    private boolean bValue = false;
    private boolean bIngredients = false;

    public List<Candy> getCandies() {
        return this.candies;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candy = new Candy();
        } else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("energy")) {
            bEnergy = true;
        } else if (qName.equalsIgnoreCase("prodaction")) {
            bProdaction = true;
        } else if (qName.equalsIgnoreCase("shugar")) {
            bShugar = true;
        } else if (qName.equalsIgnoreCase("chocolate")) {
            bChocolate = true;
        } else if (qName.equalsIgnoreCase("nut")) {
            bNut = true;
        } else if (qName.equalsIgnoreCase("water")) {
            bWater = true;
        } else if (qName.equalsIgnoreCase("proteins")) {
            bProteins = true;
        } else if (qName.equalsIgnoreCase("fats")) {
            bFats = true;
        } else if (qName.equalsIgnoreCase("carbonohydrates")) {
            bCarbohydrates = true;
        } else if (qName.equalsIgnoreCase("value")) {
            bValue = true;
        } else if (qName.equalsIgnoreCase("ingridients")) {
            bIngredients = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candies.add(candy);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bName) {
            candy.setName(new String(ch, start, length));
            bName = false;
        } else if (bType) {
            candy.setType(new String(ch, start, length));
            bType = false;
        } else if (bEnergy) {
            int energy = Integer.parseInt(new String(ch, start, length));
            candy.setEnergy(energy);
            bEnergy = false;
        } else if (bProdaction) {
            String prodaction = new String(ch, start, length);
            candy.setProdaction(prodaction);
            bProdaction = false;
        } else if (bShugar) {
            double shugar = Double.parseDouble(new String(ch, start, length));
            ingredients.setShugar(shugar);
            bShugar = false;
        } else if (bChocolate) {
            double chocolate = Double.parseDouble(new String(ch, start, length));
            ingredients.setShugar(chocolate);
            bShugar = false;
        } else if (bNut) {
            double nut = Double.parseDouble(new String(ch, start, length));
            ingredients.setShugar(nut);
            bShugar = false;
        } else if (bWater) {
            double water = Double.parseDouble(new String(ch, start, length));
            ingredients.setShugar(water);
            bShugar = false;
        } else if (bProteins) {
            double proteins = Double.parseDouble(new String(ch, start, length));
            value.setProteins(proteins);
            bProteins = false;
        } else if (bFats) {
            double fats = Double.parseDouble(new String(ch, start, length));
            value.setProteins(fats);
            bFats = false;
        } else if (bCarbohydrates) {
            double carbonohydrates = Double.parseDouble(new String(ch, start, length));
            value.setProteins(carbonohydrates);
            bCarbohydrates = false;
        } else if (bValue) {
            candy.setValue(new String(ch, start, length));
            ;
            bValue = false;
        } else if (bIngredients) {
            candy.setIngredients(new String(ch, start, length));
            bIngredients = false;
        }
    }
}
