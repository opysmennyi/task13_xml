package homework.task1.parser.dom;

import homework.task1.model.Candy;
import homework.task1.model.Ingridients;
import homework.task1.model.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DomReader {

    public List<Candy> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Candy> candies = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("beer");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
                candy.setEnergy(Integer.parseInt(element.getAttribute("energy")));
                candy.setType(element.getElementsByTagName("type").item(0).getTextContent());
                candy.setProdaction(element.getElementsByTagName("prodaction").item(0).getTextContent());
            }
        }
        return candies;
    }

    private Ingridients getIngridients(NodeList nodes) {
        Ingridients ingridients = new Ingridients();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            ingridients.setShugar(Double.parseDouble(element.getElementsByTagName("shugar").item(0).getTextContent()));
            ingridients.setChocolate(Double.parseDouble(element.getElementsByTagName("chocolate").item(0).getTextContent()));
            ingridients.setNut(Double.parseDouble(element.getElementsByTagName("nut").item(0).getTextContent()));
            ingridients.setWater(Double.parseDouble(element.getElementsByTagName("water").item(0).getTextContent()));
        }
        return ingridients;
    }

    private Value getvalue(NodeList nodes) {
        Value value = new Value();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            value.setProteins(Double.parseDouble(element.getElementsByTagName("proteins").item(0).getTextContent()));
            value.setFats(Double.parseDouble(element.getElementsByTagName("fats").item(0).getTextContent()));
            value.setCarbohydrates(Double.parseDouble(element.getElementsByTagName("carbohydrates").item(0).getTextContent()));
        }

        return value;
    }
}
