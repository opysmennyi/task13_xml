package homework.task1.parser.dom;

import homework.task1.model.Candy;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class DomParser {
    public static List<Candy> getBeerList(File xml, File xsd){
        CreateDom createDom = new CreateDom(xml);
        Document doc = createDom.getDocument();


        DomReader reader = new DomReader();

        return reader.readDoc(doc);
    }
}