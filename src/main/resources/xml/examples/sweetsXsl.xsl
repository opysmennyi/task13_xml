<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xml:base="sweetsXml.xml">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Candies</h2>
                <table border="1">
                    <tr bgcolor="blue">
                        <th>Name</th>
                        <th>Energy</th>
                        <th>Type</th>
                        <th>Shugar</th>
                        <th>Chocolate</th>
                        <th>Nut</th>
                        <th>Water</th>
                        <th>Proteins</th>
                        <th>Fats</th>
                        <th>Carbohydrates</th>
                        <th>Prodaction</th>
                    </tr>
                    <xsl:for-each select="candies/candy">
                        <tr>
                        <td bgcolor="gold"><xsl:value-of select="name"/></td>
                        <td bgcolor="red" ><xsl:value-of select="energy"/></td>
                        <td><xsl:value-of select="type"/></td>
                        <!--<td><xsl:value-of select="ingridients"/></td>-->
                        <td bgcolor="yellow"><xsl:value-of select="ingridients/shugar"/></td>
                        <td bgcolor="yellow"><xsl:value-of select="ingridients/chocolate"/></td>
                        <td bgcolor="yellow"><xsl:value-of select="ingridients/nut"/></td>
                        <td bgcolor="yellow"><xsl:value-of select="ingridients/water"/></td>
                        <!--<td><xsl:value-of select="value"/></td>-->
                        <td bgcolor="yellow"><xsl:value-of select="value/proteins"/></td>
                        <td bgcolor="yellow"><xsl:value-of select="value/fats"/></td>
                        <td bgcolor="yellow"><xsl:value-of select="value/carbohydrates"/></td>
                        <td bgcolor="green"><xsl:value-of select="prodaction"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>